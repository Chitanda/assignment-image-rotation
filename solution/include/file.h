#include "structs.h"
#include <stddef.h>
#include <stdio.h>

enum load_status{
    OK,
    DTOK

};
enum image_write_status{
    OKAY,
    FAILED
};

enum load_status load(FILE* Source, struct Image* New);
enum image_write_status create(FILE* out, struct Image* Source);
uint32_t padding_create(struct Image* Source);



