#pragma once

#include <stdint.h>


#pragma pack(push, 1)
struct Rgb{
    uint8_t b, g, r;
};
#pragma pack(pop)


struct Image{
    uint32_t width, height;
    struct Rgb* colors;
};

#pragma pack(push, 1)
struct Header{
    unsigned char BM[2];
    uint32_t biSize;
    uint32_t Unused;
    uint32_t biOffset;
    uint32_t sizeHeader;
    uint32_t width;
    uint32_t height;
    uint16_t planes;
    uint16_t bits;
    uint32_t compression;
    uint32_t sizeImage;
    uint32_t xPerMeter;
    uint32_t yPerMeter;
    uint32_t clrUsed;
    uint32_t indColor;
};
#pragma pack(pop)






