#include "../include/rotation.h"
#include "../include/image.h"
#include <malloc.h>

struct Image* rotate(struct Image* const src){
    long long width = src->width;
    long long height = src->height;
    struct Image* new = alloc_image(height, width);
    for(size_t i = 0; i < src->width; i++) {
        for(size_t j = 0; j < src->height; j++) {
            new->colors[i * height + j] =  src->colors[(height - 1 - j) * width + i];
        }
    }
    return new;
}
