#include "../include/file.h"
#include "../include/rotation.h"
#include "../include/structs.h"
#include <malloc.h>
#include <stdio.h>
#define PLANES 1
#define DB_SIZE 40
#define BPP 24
#define COM 0
#define X_PPM 0
#define Y_PPM 0
#define IMP_COLOR 0
#define NUM_COLOR 0
#define SIZE 54 
uint32_t padding_create(struct Image* Source) {
    uint32_t padding = (uint32_t)(4 - ((3 * Source->width) % 4)) % 4;
    return padding;
}
struct Header default_header (struct Image* Source) {
struct Header header = {
        "BM",
        SIZE + Source->width * Source->height * 3 + Source->height * padding_create(Source),
        0,
        SIZE,
        DB_SIZE,
        Source->width,
        Source->height,
        PLANES,
        BPP,
        COM,
        Source->width * Source->height * 3 + Source->height * padding_create(Source),
        X_PPM,
        Y_PPM,
        NUM_COLOR,
        IMP_COLOR
    };
    return header;
}

void init_image (struct Image* New, struct Header header) {
    New->width = header.width;
    New->height = header.height;
    New->colors = malloc(sizeof(struct Rgb) * header.height * header.width);
}

enum image_write_status create(FILE* out,struct Image* Source) {
struct Header header = default_header(Source);
    if (fwrite(&header, sizeof(header), 1, out) != 1) return FAILED;

    size_t zero = 0;
    long  long padding = padding_create(Source);
    for (uint32_t x = 0; x < Source->height; ++x) {
        if(fwrite(&Source->colors[x * Source->width], sizeof(struct  Rgb), Source->width, out) == 0) return FAILED;
        if(fwrite(&zero, padding, 1, out) == 0) return FAILED;
    }
    return OKAY;
}


enum load_status load(FILE* Source, struct Image* New){
    struct Header Header;
    fread(&Header, sizeof(Header), 1, Source);
    init_image (New,Header);
    uint32_t padding = padding_create(New);
    for(size_t i = 0; i < Header.height; i++){
        if(fread(&New->colors[i * Header.width], sizeof(struct Rgb), Header.width, Source) == 0) return DTOK;
        if(fseek(Source, padding, SEEK_CUR) != 0) return DTOK;
    }

    return OK;
}






