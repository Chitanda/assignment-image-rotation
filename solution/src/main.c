#include "../include/file.h"
#include "../include/image.h"
#include "../include/rotation.h"
#include <malloc.h>
#include <stdio.h>

int main(int argc, char* argv[]) {
    if (argc >= 3) {
        FILE *Src = fopen(argv[1], "rb");

        struct Image *Im = malloc(sizeof(struct Image));

        load(Src, Im);

        struct Image* rot = rotate(Im);

        FILE* out = fopen(argv[2], "wb");
        create(out, rot);

        fclose(Src);
        fclose(out);

        free_alloc_memory(Im);
        free_alloc_memory(rot);
        printf("успех\n");
    }
    else {
        printf("error");
    }
}

