#include "../include/image.h"
#include "../include/structs.h"
#include <malloc.h>


void free_alloc_memory(struct Image* ima){
    free(ima->colors);
    free(ima);
}

struct Image* alloc_image(unsigned long w, unsigned long h){
    struct Image* ima = malloc(sizeof(struct Image));
    ima->width = w;
    ima->height = h;
    ima->colors = malloc(sizeof(struct Rgb) * h*w);
    return ima;
}





